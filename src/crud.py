from sqlalchemy.orm import Session
from passlib.context import CryptContext
from datetime import datetime

from . import models
from . import schemas

# TODO: Vidjeti dodatne opcije pri sifrovanju tipa salt itd.
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def get_user(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_email(db: Session, email: str):
    return db.query(models.User).filter(models.User.email == email).first()


def get_user_by_username(db: Session, username: str):
    return db.query(models.User).filter(models.User.username == username).first()


def get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.User).offset(skip).limit(limit).all()


def create_user(db: Session, user: schemas.UserCreate):
    hashed_password = pwd_context.hash(user.password)

    db_user = models.User(
        email=user.email,
        username=user.username,
        hashed_password=hashed_password,
        is_admin=False,
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)

    return db_user


def check_credentials(db: Session, user: schemas.UserLogin):
    # Ucitavamo korisnika da bismo provjerili tacnost podataka
    db_user = (
        db.query(models.User).filter(models.User.username == user.username).first()
    )

    # Korisnik ne postoji
    if not db_user:
        return None

    # Password je tacan
    if pwd_context.verify(user.password, db_user.hashed_password):
        return db_user

    # Password je netacan
    return None


def get_plates(db: Session, user: schemas.User):
    if user.is_admin:
        return db.query(models.LicensePlate).all()

    return (
        db.query(models.LicensePlate)
        .filter(models.LicensePlate.owner_id == user.id)
        .all()
    )


def get_plate(db: Session, number: str, active: bool = True):
    return (
        db.query(models.LicensePlate)
        .filter(
            models.LicensePlate.number == number,
            models.LicensePlate.is_active == active,
        )
        .first()
    )


def create_user_plate(
    db: Session, plate: schemas.LicensePlateCreate, user: schemas.User
):
    # Ako je admin automatski "aktivirati" registarsku oznaku
    db_plate = models.LicensePlate(
        **plate.dict(), is_active=user.is_admin, owner_id=user.id
    )
    db.add(db_plate)
    db.commit()
    db.refresh(db_plate)

    return db_plate


def update_plate(db: Session, plate_id: int, active: bool):
    db_plate = (
        db.query(models.LicensePlate).filter(models.LicensePlate.id == plate_id).first()
    )

    db_plate.is_active = active

    db.commit()

    return db_plate


def delete_plate(db: Session, plate_id: int):
    count = (
        db.query(models.LicensePlate)
        .filter(models.LicensePlate.id == plate_id)
        .delete(synchronize_session=False)
    )

    db.commit()
    return count


def create_entry(db: Session, plate_id: int):
    db_entry = models.Entry(plate_id=plate_id)
    db.add(db_entry)
    db.commit()
    db.refresh(db_entry)

    return db_entry


def read_active_entries(db: Session):
    return db.query(models.Entry).filter(models.Entry.datetime_out == None).all()


def read_entries(db: Session, user: schemas.User):
    if user.is_admin:
        return db.query(models.Entry).all()

    return (
        db.query(models.Entry)
        .join(models.LicensePlate)
        .join(models.User)
        .filter(models.User.id == user.id)
        .all()
    )


def read_active_entry(db: Session, plate_id: int):
    return (
        db.query(models.Entry)
        .filter(models.Entry.plate_id == plate_id, models.Entry.datetime_out == None)
        .first()
    )


def close_active_entry(db: Session, plate_id: int):
    db_entry = (
        db.query(models.Entry)
        .filter(models.Entry.plate_id == plate_id, models.Entry.datetime_out == None)
        .first()
    )

    if db_entry is None:
        # Nema aktivnog ulaska
        return False

    db_entry.datetime_out = datetime.utcnow()

    db.commit()

    return True


def update_entry(db: Session, plate_number: str):
    # Npr. kada vozilo napusti parking treba da upisemo vrijeme napustanja
    db_entry = (
        db.query(models.Entry)
        .join(models.LicensePlate)
        .filter(
            models.LicensePlate.number == plate_number,
            models.Entry.datetime_out == None,
        )
        .first()
    )

    db_entry.datetime_out = datetime.utcnow()
    db.commit()

    return db_entry


def get_variable(db: Session, name: str):
    return db.query(models.Variable).filter(models.Variable.name == name).first()


def increment_car_count(db: Session, value=1):
    v = db.query(models.Variable).filter(models.Variable.name == "car_count").first()

    # Uvecaj brojac auta i opet sacuvaj kao string
    v.value = str(int(v.value) + value)

    db.commit()


def set_car_count(db: Session, value):
    # Za rucno podesavanje broja vozila na parkingu
    v = db.query(models.Variable).filter(models.Variable.name == "car_count").first()

    v.value = str(value)

    db.commit()
