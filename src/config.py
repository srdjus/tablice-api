from pydantic import BaseSettings

class Settings(BaseSettings):
    session_secret: str
    access_token_expire_minutes: int
    algorithm: str

    class Config:
        env_file = ".env"