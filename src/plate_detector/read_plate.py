"""
Just a first shot at this, more flexible solution needed for the future.
"""
import easyocr
import re

reader = easyocr.Reader(["en"])


def read_plate(image, logging: bool = False):
    ocr_result = reader.readtext(image)

    if logging:
        print(f"OCR output: // f{ocr_result} //\n")

    text = ""

    # TODO: Check probability and then return parsed text
    for _, content, prob in ocr_result:
        text += f"{content} "

    # TODO: Separate patterns for car plate variations (taxis, gov, military...)
    # TODO: 0/O in the middle is bit weird

    # This part is commented out, for the sake of versatility, for now returning raw text
    # **********
    # bh_plate_pattern = re.compile("[AEJKMOT0-9][0-9]{2}-[AEJKMOT0]-[0-9]{3}")

    # results = bh_plate_pattern.findall(text)

    # if len(results):
    #    return results[0]
    # else:
    #    return None
    # **********

    return text
