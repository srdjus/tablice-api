from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
from datetime import datetime

from .database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    username = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    is_admin = Column(Boolean)
    datetime_created = Column(DateTime, server_default=func.now())

    plates = relationship("LicensePlate", back_populates="owner")


class LicensePlate(Base):
    __tablename__ = "plates"

    id = Column(Integer, primary_key=True, index=True)
    number = Column(String, unique=True, index=True)
    owner_id = Column(Integer, ForeignKey("users.id"))
    is_active = Column(Boolean)

    owner = relationship("User", back_populates="plates")


class Entry(Base):
    __tablename__ = "entries"

    id = Column(Integer, primary_key=True, index=True)
    plate_id = Column(Integer, ForeignKey("plates.id"))
    datetime_in = Column(DateTime, default=func.now())
    datetime_out = Column(DateTime)

    plate = relationship("LicensePlate")


class Variable(Base):
    __tablename__ = "variables"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    value = Column(String)
