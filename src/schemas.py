from pydantic import BaseModel, Field, validator
from datetime import datetime

# Common class
class UserBase(BaseModel):
    email: str = Field(
        default=None, title="E-mail address", description="Must contain @ character",
    )

    def __init__(self, **data):
        data["email"] = data["email"].lower()
        super().__init__(**data)

    @validator("email")
    def email_must_contain_email_sign(cls, v):
        if "@" not in v:
            raise ValueError("Must contain '@' character")
        return v
    

# Ocvo samo za registraciju
class UserCreate(UserBase):
    username: str = Field(
        default=None,
        min_length=3,
        max_length=10,
        title="Username",
        description="Lowercase, alphanumeric, unique username",
    )
    password: str = Field(
        default=None,
        min_length=3,
        title="Password",
        description="At least 3 characters long",
    )

    @validator("username")
    def username_must_be_alphanumeric(cls, v):
        if not v.isalnum():
            raise ValueError("Must be alphanumeric")
        return v

    @validator("username")
    def username_must_be_lowercase(cls, v):
        if not v.islower():
            raise ValueError("Must be lowercase")
        return v


# Za login (trenutno isto sto i za registraciju sem validatora)
class UserLogin(BaseModel):
    username: str
    password: str


# Sve sem passworda
class User(UserBase):
    id: int
    username: str
    is_admin: bool
    datetime_created: datetime

    class Config:
        orm_mode = True


class UserInDB(User):
    password: str


class LicensePlateBase(BaseModel):
    number: str = Field(
        regex=r"^[AEJKMOT0-9][0-9]{2}-[AEJKMOT]-[0-9]{3}$",
        title="License plate number",
        description="Should be a valid license plate",
    )


class LicensePlateCreate(LicensePlateBase):
    pass


class LicensePlate(LicensePlateBase):
    id: int
    owner_id: int
    owner: User
    is_active: bool

    class Config:
        orm_mode = True


class EntryCreate(BaseModel):
    plate_id: int

    class Config:
        orm_mode = True


class Entry(EntryCreate):
    id: int
    plate: LicensePlate
    datetime_in: datetime
    datetime_out: datetime | None = None


class Variable(BaseModel):
    name: str
    value: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None
