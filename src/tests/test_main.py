# NOTE: test.db mora da se obrise prije pokretanja testa
# TODO: Ukloniti duple logine 
from cgitb import text
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from jose import jwt
from ..config import Settings

from ..database import Base
from ..main import app, get_db

SQLALCHEMY_DATABASE_URL = "sqlite:///./test.db"

settings = Settings() # Ovako samo za testing 

engine = create_engine(SQLALCHEMY_DATABASE_URL, connect_args={
                       "check_same_thread": False})

TestingSessionLocal = sessionmaker(
    autocommit=False, autoflush=False, bind=engine)

Base.metadata.create_all(bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)


def test_create_user_success():
    response = client.post(
        "/users/", json={"email": "me@batman.com", "username": "batman", "password": "dontneedone"})

    assert response.status_code == 200, response.text

    data = response.json()
    assert data["username"] == "batman"
    assert "id" in data

    response = client.get("/users/", params={"username": "batman"})
    assert response.status_code == 200, response.text
    data = response.json()
    assert "exist" in data
    assert data["exist"]


def test_create_user_invalid():
    # Lista nepravilnih korisnickih imena
    invalid_usernames = ["aa", "toomanycharacters",
                         ".withdot", " withspace", "BATMAN"]

    for username in invalid_usernames:
        response = client.post(
            "/users/", json={"email": "me@batman.com", "username": username, "password": "dontneedone"}
        )

        # Treba da vrati gresku za validaciju
        assert response.status_code == 422, response.text

    # Pokusavanje sa zauzetim prezimenom
    response = client.post(
        "/users/", json={"email": "new@batman.com", "username": "batman", "password": "dontneedone"}
    )

    assert response.status_code == 400, response.text
    assert response.json() == {"detail": "Username already registered"}

    # Pokusavanje sa zauzetim korisnickim imenom
    response = client.post(
        "/users/", json={"email": "me@batman.com", "username": "robin", "password": "dontneedone"}
    )

    assert response.status_code == 400, response.text
    assert response.json() == {"detail": "E-mail already registered"}

    # Pokusaj sa prekratkim korisnickim imenom
    response = client.post(
        "/users/", json={"email": "me@batman", "username": "batman", "password": "ab"})

    assert response.status_code == 422, response.text


def test_login_for_access_token():
    # Pokusati sa netacnim podacima
    response = client.post(
        "/token/", data={"username": "batman", "password": "fakepassword"})
    assert response.status_code == 400, response.text
    assert response.json() == {"detail": "Incorrect username or password"}

    # Pokusati sa validnim podacima
    response = client.post(
        "/token/", data={"username": "batman", "password": "dontneedone"})

    assert response.status_code == 200, response.text
    data = response.json()

    assert "access_token" in data
    assert "token_type" in data

    # Provjeriti da li je token validan
    token = data["access_token"]
    payload = jwt.decode(token, settings.session_secret,
                         algorithms=[settings.algorithm])
    assert payload["sub"] == "batman"


def test_read_users_me_no_token():
    response = client.get("/users/me/")

    assert response.status_code == 401
    assert response.json() == {"detail": "Not authenticated"}


def test_read_plates_no_token():
    response = client.get("/plates/")

    assert response.status_code == 401
    assert response.json() == {"detail": "Not authenticated"}


def test_create_plate_no_token():
    response = client.post("/plates/", json={"number": "A11-A-111"})

    assert response.status_code == 401
    assert response.json() == {"detail": "Not authenticated"}


def test_create_plate_valid():
    # Login kako bi dobili token
    response = client.post(
        "/token/", data={"username": "batman", "password": "dontneedone"})

    assert response.status_code == 200, response.text
    data = response.json()

    assert "access_token" in data
    assert "token_type" in data

    token = data["access_token"]

    # Kreiranje tablice
    response = client.post(
        "/plates/", json={"number": "A11-A-222"}, headers={"Authorization": f"Bearer {token}"}
    )

    assert response.status_code == 200, response.text
    data = response.json()

    assert data["number"] == "A11-A-222"
    assert "id" in data
    assert "owner_id" in data


def test_create_plate_invalid():
    invalid_plate_numbers = ["", "1234567", "A12-B-345", "A12-AA-345",
                             "123-A--456", "AA1-A-333", "J32-M-1555", "*"]

    # Login zbog tokena
    response = client.post(
        "/token/", data={"username": "batman", "password": "dontneedone"})

    assert response.status_code == 200, response.text
    data = response.json()

    assert "access_token" in data
    assert "token_type" in data

    token = data["access_token"]

    # TODO: Testirati i description polje
    for plate_number in invalid_plate_numbers:
        response = client.post(
            "/plates/", json={"number": plate_number}, headers={"Authorization": f"Bearer {token}"})

        assert response.status_code == 422, response.text
