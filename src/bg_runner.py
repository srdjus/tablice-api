import serial
import serial.tools.list_ports
import cv2
from src.plate_detector_torch.load_model import run_plate_detector
import asyncio
import concurrent.futures
from .database import SessionLocal
from . import crud


class BackgroundRunner:
    def __init__(self, with_device=False):

        print("Initializing background runner...")

        self.loop = asyncio.get_running_loop()

        # Ovo je potrebno kako bi se mogla zaustaviti beskonacna petlja
        self.is_processing = False

        # Kad se detektuju tablice varijable postavi na True sto znaci da treba
        # sacekati da automobil udje, dok je to tacno prepoznavanje se ne vrsi
        self.car_detected = False

        # id zadnje detektovane tablice
        self.last_det_id = -1

        self.arduino = None

        if with_device:
            # Pronalazenje porta na kom je arduino
            ports = serial.tools.list_ports.comports()

            device = None

            for port in ports:
                if port.manufacturer == "Arduino (www.arduino.cc)":
                    device = port.device

            if not device:
                print("Arduino not available.")

            self.arduino = serial.Serial(baudrate=115200, port=device, timeout=1)

    async def process(self):
        self.start_processing()

        # Snimanje se pokrece u drugom threadu
        with concurrent.futures.ThreadPoolExecutor() as executor:
            await self.loop.run_in_executor(executor, self.work)

    def work(self):
        # Pokretanje snimanja (datoteka ili kamera)
        cap = cv2.VideoCapture("assets/video1.mp4")

        if not cap.isOpened():
            # TODO: Handle error
            print("Greška: Neuspjelo pokretanje kamere.")
            self.is_processing = False

        while self.is_processing:
            # Detektujemo auta jer auto trenutno ne prolazi
            if not self.car_detected:

                # Rucno za testiranje
                #print("Unesite tablice: ")
                #det_num = input()
                #plate_numbers = [det_num]

                ret, frame = cap.read()

                if not ret:
                    self.is_processing = False
                    break

                # Pokrenemo detektor
                frame_with_detections, plate_numbers = run_plate_detector(frame)

                # Prikazati sliku na ekranu samo za potrebe testiranje
                cv2.imshow("Camera input", frame_with_detections)

                for det_num in plate_numbers:

                    # Provjerimo da li su tablice u bazi i ako jesu sacuvamo njihov id
                    # kako bi se u slucaju da auto uspjesno udje mogao unijeti ulazak (entry)
                    db = SessionLocal()
                    plate_temp = crud.get_plate(db, det_num)
                    db.close()

                    if plate_temp is not None:
                        # Tablica sa datim oznakama postoji u bazi
                        self.car_detected = True
                        self.last_det_id = plate_temp.id

                        if self.arduino is not None:
                            # Saljemo preko serijskog kod ili signal na Raspberry Pi digital output
                            self.arduino.write(b"OPEN")

                if cv2.waitKey(1) == ord("q"):
                    self.is_processing = False
                    break
            
            read_msg_serial = ""

            if self.car_detected:
                # Detektovan auto pa citamo vrijednosti sa senzora
                if self.arduino is not None:
                    read_msg_serial = self.arduino.readline().decode()

                # Rucno testiranje ako Arduino nije spojen 
                # read_msg_serial = input()

            if self.car_detected and read_msg_serial == "PASS":
                print(f"Automobil prosao (id: {self.last_det_id})")

                db = SessionLocal()

                # Prvo provjerimo da li je ovaj automobil vec na parkingu
                active_entry = crud.read_active_entry(db, self.last_det_id)

                if active_entry is not None:
                    crud.close_active_entry(db, self.last_det_id)
                    print("Zatvoren postojeci aktivni ulazak.")

                crud.create_entry(db, self.last_det_id)
                print(f"Kreiran novi ulazak za plate_id: {self.last_det_id}")

                # Povecaj brojac i sacuvaj u bazu
                print(
                    f"Sacuvaj u bazu da je plate id {self.last_det_id} usao na parking"
                )

                # Restuj flag kako bi se mogao detektovati novi ulazak
                self.car_detected = False

                if self.arduino is not None:
                    self.arduino.write(b"PASSED")

                break

        # Zatvranje prozora
        cap.release()
        cv2.destroyAllWindows()

    def stop_processing(self):
        # Kraj snimanja
        self.is_processing = False

    def start_processing(self):
        self.is_processing = True
