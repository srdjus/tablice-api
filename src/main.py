import asyncio
from datetime import datetime, timedelta
from fastapi import Body, FastAPI, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from sqlalchemy.orm import Session
from functools import lru_cache
from . import crud, models, schemas
from .database import SessionLocal, engine
from .config import Settings

from .bg_runner import BackgroundRunner

# Uvezati SQLAlchemy
models.Base.metadata.create_all(bind=engine)

app = FastAPI()


@lru_cache()
def get_settings():
    # Ucitati varijable okruzenja
    return Settings()


def get_db():
    """Ucitavanje baze"""
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


# OAuth
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    """
    Kreira token nakon sto se korisnik uspjesno uloguje
    Args:
    data -- dictionary sa "sub" kljucem koji je u stvari user id (definise JWT)
    expires_delta -- vrijeme trajanja sesije u milisekundama
    Returns:
    encoded_jwt -- vrijednost tokena
    """
    to_encode = data.copy()

    # Racunanje datuma isteka
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)

    #  Dodavanje datuma isteka u dictionary
    to_encode.update({"exp": expire})

    settings = get_settings()

    encoded_jwt = jwt.encode(
        to_encode, settings.session_secret, algorithm=settings.algorithm
    )

    return encoded_jwt


def get_current_user(
    token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)
):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )

    try:
        settings = get_settings()

        # Iscitivanje podataka iz tokena
        payload = jwt.decode(
            token, settings.session_secret, algorithms=[settings.algorithm]
        )

        username: str = payload.get("sub")

        # Prazno sto znaci da nema sesije, niko nije ulogovan
        if username is None:
            raise credentials_exception

        token_data = schemas.TokenData(username=username)
    except JWTError:
        raise credentials_exception

    user = crud.get_user_by_username(db, token_data.username)

    # Korisnik nije pronadjen u bazi podataka
    if user is None:
        raise credentials_exception

    # Posto je sesija pronadjena vratiti je
    return user


def get_current_admin_user(current_user: schemas.User = Depends(get_current_user)):
    """Provjera da li je korisnicki nalog aktiviran"""
    if not current_user.is_admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Not an admin"
        )

    return current_user


recognition_runner = BackgroundRunner(with_device=True)


@app.on_event("startup")
async def app_startup():
    # Pokrece se simultano proces za prepoznavanje tablica
    asyncio.create_task(recognition_runner.process())


@app.on_event("shutdown")
async def app_shutdown():
    recognition_runner.stop_processing()

    # Sacekati malo prije nego sto se thread zatvori
    await asyncio.sleep(1)


# Login ruta
@app.post("/token/", response_model=schemas.Token, summary="Login, dobijanje tokena")
def login_for_access_token(
    form_data: OAuth2PasswordRequestForm = Depends(),
    db: Session = Depends(get_db),
    settings: Settings = Depends(get_settings),
):
    bad_request_exception = HTTPException(
        status_code=status.HTTP_400_BAD_REQUEST, detail="Incorrect username or password"
    )

    user = crud.check_credentials(
        db, schemas.UserLogin(username=form_data.username, password=form_data.password)
    )

    # Korisnik nije pronadje los
    if not user:
        raise bad_request_exception

    # Tacni podaci kreiranje tokena
    access_token_expires = timedelta(minutes=settings.access_token_expire_minutes)

    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )

    # TODO: Vratiti i korisnicke podatke
    return {"access_token": access_token, "token_type": "bearer"}


# Vraca korisnicki profil
@app.get(
    "/users/me/",
    response_model=schemas.User,
    summary="Vrati podatke o trenutnom korisniku",
)
def read_users_me(current_user: schemas.User = Depends(get_current_user)):
    return current_user


# Trenutno vraca samo da li je korisnik registrovan ili ne
@app.get("/users/", summary="Vraća da li je korisnik registrovan (samo za test)")
def read_user(
    db: Session = Depends(get_db), email: str | None = None, username: str | None = None
):
    if email:
        user: schemas.UserBase = crud.get_user_by_email(db, email)
        if user:
            return {"exist": True}

        return {"exist": False}
    if username:
        user: schemas.UserBase = crud.get_user_by_username(db, username)
        if user:
            return {"exist": True}

        return {"exist": False}

    return {}


# Registracija
@app.post("/users/", response_model=schemas.User, summary="Registracija")
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_email(db, email=user.email)

    if db_user:
        raise HTTPException(
            status.HTTP_400_BAD_REQUEST, detail="E-mail already registered"
        )

    db_user = crud.get_user_by_username(db, username=user.username)

    if db_user:
        raise HTTPException(
            status.HTTP_400_BAD_REQUEST, detail="Username already registered"
        )

    return crud.create_user(db=db, user=user)


@app.post("/plates/", response_model=schemas.LicensePlate, summary="Dodavanje tablice")
def create_plate(
    plate: schemas.LicensePlateCreate,
    current_user: schemas.User = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    return crud.create_user_plate(db, plate=plate, user=current_user)


@app.get(
    "/plates/",
    response_model=list[schemas.LicensePlate] | schemas.LicensePlate,
    summary="Čitanje tablica",
)
def read_plates(
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Ako je korisnik admin, vraća sve tablice, inače samo one koje pripadaju korisniku."""
    return crud.get_plates(db, user=current_user)


@app.put(
    "/plates/{plate_id}/activate/",
    dependencies=[Depends(get_current_admin_user)],
    response_model=schemas.LicensePlate,
    summary="Aktiviranje ili deaktiviranje tablice",
)
def activate_plate(
    plate_id: int, value: bool = Body(embed=True), db: Session = Depends(get_db)
):
    return crud.update_plate(db, plate_id, active=value)


@app.post(
    "/entries/",
    dependencies=[Depends(get_current_admin_user)],
    response_model=schemas.Entry,
    summary="Dodavanje prolaza",
)
def create_entry(plate_id: int, db: Session = Depends(get_db)):
    return crud.create_entry(db, plate_id)


@app.get(
    "/entries/",
    response_model=list[schemas.Entry],
    summary="Izlistavanje svih prolazaka",
)
def read_entries(
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Ako je korisnik admin vraća sve, inače samo one koji pripadaju korisniku."""
    return crud.read_entries(db, user=current_user)


@app.get(
    "/entries/active/",
    response_model=list[schemas.Entry],
    dependencies=[Depends(get_current_admin_user)],
    summary="Izlistavanje aktivnih prolazaka",
)
def read_active_entries(db: Session = Depends(get_db)):
    """Trenutno samo administrator ima mogućnost korišćenja rute. Vraća sve aktivne prolaske."""
    return crud.read_active_entries(db)


@app.get(
    "/entries/active/{plate_id}",
    dependencies=[Depends(get_current_admin_user)],
    response_model=schemas.Entry,
    summary="Čitanje aktivnog ulaska (test)",
)
def read_active_entry(plate_id: int, db: Session = Depends(get_db)):
    """
    Parametri:
    plate_id - id tablice za koju provjeravamo da li ima aktivni ulazak i ako ima vratimo ga
    """

    active_entry = crud.read_active_entry(db, plate_id)

    if active_entry is None:
        raise HTTPException(404, "Date tablice nemaju aktivan ulazak.")

    return active_entry


# Ovo se koristi samo zarad testiranja, ulasci se ne azuriraju preko API-ja nego iz skirpte za prepoznavanje
@app.put(
    "/entries/",
    dependencies=[Depends(get_current_admin_user)],
    summary="Ažuriranje prolaska",
    response_model=schemas.Entry,
)
def update_entry(plate_number: str = Body(embed=True), db: Session = Depends(get_db)):
    updated_entry = crud.update_entry(db, plate_number)

    if updated_entry is None:
        raise HTTPException(400, "Nemoguce azurirati ulazak za date tablice.")

    return crud.update_entry(db, plate_number)


@app.put(
    "/entries/active/{plate_id}",
    dependencies=[Depends(get_current_admin_user)],
    summary="Zatvaranje aktivnog ulaska (test)",
)
def close_active_entry(plate_id: int, db: Session = Depends(get_db)):
    return crud.close_active_entry(db, plate_id)


@app.get(
    "/count/",
    dependencies=[Depends(get_current_user)],
    summary="Čitanje broja automobila na parkingu",
)
def get_count(db: Session = Depends(get_db)):
    return crud.get_variable(db, "car_count")


@app.put(
    "/count/",
    dependencies=[Depends(get_current_user)],
    summary="Setovanje broja automobila na parkingu",
)
def update_count(value: str = Body(embed=True), db: Session = Depends(get_db)):
    return crud.set_car_count(db, value)
