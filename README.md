## Bellbot API [sr]
API za sisteme automatskog registrovanja registarskih oznaka. Na videu je prikaz simulacije (direktorijum *examples*) integraciji sa mobilnom aplikacijom.

<figure class="video_container">
  <video controls="true" allowfullscreen="true">
    <source src="examples/simulation.webm" type="video/webm">
    <source src="examples/simulation.mp4" type="video/mp4">
  </video>
</figure>

Sistem se sastoji od dva glavna dijela, REST servera te skripte za detekciju objekata. 
### Server
U osnovi server komunicira sa klijentima (u opštem slučaju Bellbot android aplikacija) koristeći FastAPI framework u kombinaciji sa SQLite bazom podataka (SQLAlchemy ORM). Korisničke sesije zasnivaju se na implementaciji JWT-a (JSON Web Tokens).

Osnovne funkcije:
- korisnički nalozi,
- korisničke sesije,
- upravljanje registarskim oznakama i 
- upravljanje režimima.

### Detekcija objekata
U slučaju da korisnička konfiguracija koristi automatski režim (*to znači da se signal šalje uređaju kada se detektuju definisane registarske oznake*) simultano sa serverom izvršava se i skripta za detekciju objekata. 


### Pokretanje
API se pokreće sljedećom komandom:
```
uvicorn --host 0.0.0.0 src.main:app --reload
```
